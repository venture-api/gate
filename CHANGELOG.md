[0.3.1] - 2018-06-21
--------------------
- [x] Increase code coverage


[0.3.0] - 2018-06-21
--------------------
- [x] Move to Kojo + Fastify
- [x] Add factory creation route


[0.2.1] - 2018-06-12
--------------------
- [x] Move to GitLab


[0.2.0] - 2018-04-11
--------------------
- [x] Add player auth/registration via google #4
- [x] Add tests
